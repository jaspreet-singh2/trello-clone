# Summary: Trello Clone Project

## Overview
Developed a Trello-inspired task management application as a personal project to hone my skills in full-stack development.

## Key Features Implemented

- **State Management Evolution:**
  - `master` branch: Initial state management approach.
  - `reducer` branch: Implementation using `useReducer` for improved state management.
  - `redux` branch: Utilization of Redux for sophisticated state handling.

- **Front-End Development:**
  - `master` branch: Initial front-end setup.
  - `reducer` and `redux` branches: Enhanced front-end with respective state management techniques.

- **Back-End Development:**
  - `master` and `redux` branches: Initial stages.
  - `with-local-api` branch: Integration of a local API backend using Express.js, Sequelize ORM, and PostgreSQL.

- **Tech Stack Evolution:**
  - *Front-End:* React, Hooks (useState, useReducer), Redux for state management.
  - *Back-End:* Node.js, Express.js, Sequelize ORM, PostgreSQL for database management.

- **Learning Progression:**
  - Demonstrated an evolution in understanding from basic state management to more complex solutions, showcasing adaptability and growth in handling data at scale.

- **Challenges Overcame:**
  - Adapted to transitioning between various state management techniques, resolving challenges in data flow and application scalability.
  - Successfully integrated a back-end system to the front-end, mastering full-stack development paradigms.

## Personal Learnings
- Developed a deeper understanding of front-end frameworks and state management, with a focus on React and its evolving state management paradigms.
- Gained hands-on experience in building RESTful APIs, handling database models, and ensuring smooth communication between front-end and back-end systems.

## Takeaways
- Enhanced proficiency in React, React Hooks, and state management.
- Acquired comprehensive knowledge in full-stack development, from front-end components to building and managing a relational database.
