import Boards from "./Components/Boards";
import CssBaseline from "@mui/material/CssBaseline";
import Error from "./Components/error";
import Lists from "./Components/Lists";
import Navbar from "./Components/Navbar";
import { ThemeProvider } from "@emotion/react";
import { Typography } from "@mui/material";
import { createTheme } from "@mui/material/styles";
import { BrowserRouter, Route, Routes } from "react-router-dom";

const THEME = createTheme({
  palette: {
    mode: 'dark',
    background: {
      default: "rgb(29,33,37)",
      paper: "rgb(29,33,37)"
    }
  },
  typography: {
    fontFamily: [
      '-apple-system',
      'BlinkMacSystemFont',
      '"Segoe UI"',
      'Roboto',
      '"Helvetica Neue"',
      'Arial',
      'sans-serif',
      '"Apple Color Emoji"',
      '"Segoe UI Emoji"',
      '"Segoe UI Symbol"',
    ].join(','),
    "fontSize": 16,
    "fontWeightLight": 300,
    "fontWeightRegular": 400,
    "fontWeightMedium": 500
  }
});




function App() {

  return (
    <>
      <ThemeProvider theme={THEME}>
        <CssBaseline />
        
       

        <BrowserRouter>
        <Navbar></Navbar>
       
          <Routes>
        
            <Route path="/" element={<Boards />} />
            
            <Route path='/boards/:BoardId' element={<Lists />}></Route>
           
            <Route path='*' element={<Error msg={'404 Url Not Found'} color={'error'}/>}></Route>
           
          </Routes>

        </BrowserRouter>


      </ThemeProvider>

    </>
  )
}

export default App
