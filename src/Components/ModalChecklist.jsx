import AddIcon from "@mui/icons-material/Add";
import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import CheckBoxOutlinedIcon from "@mui/icons-material/CheckBoxOutlined";
import CheckItem from "./Checkitems";
import DeleteOutlinedIcon from "@mui/icons-material/DeleteOutlined";
import Modal from "@mui/material/Modal";
import Typography from "@mui/material/Typography";
import axios from "axios";
import { IconButton, TextField } from "@mui/material";
import { useEffect, useState } from "react";

const ApiandToken = import.meta.env.VITE_KEYANDTOKEN;





const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: '500px',
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
    p: 4,
    borderRadius: '50px'
};








function CheckListModal({id, name }) {


    const [open, setOpen] = useState(false);
    const [ChecklistName, setChecklistName] = useState('');
   
    const handleOpen = () => setOpen(true);
    const [CheckLists, setCheckLists] = useState([]);
    

    useEffect(() => {

        axios.get(`https://api.trello.com/1/cards/${id}/checklists?${ApiandToken}`)
            .then(res => {

                //console.log(res.data);
                setCheckLists(res.data);
              
            })

    }, [open])


    function addChecklist()
    {
        axios.post(`https://api.trello.com/1/cards/${id}/checklists?${ApiandToken}&name=${ChecklistName}`)
        .then(res=>{
             //console.log(res.data);
            setCheckLists([...CheckLists,res.data]);
            setChecklistName('')
        })
    }

   function deleteChecklist(CheckListId)
   {
        axios.delete(`https://api.trello.com/1/cards/${id}/checklists/${CheckListId}?${ApiandToken}`).
        then(res=>{
             
            setCheckLists(CheckLists.filter(list=>list.id!==CheckListId))
        })

   }

    //console.log(CheckLists);

    const handleClose = () => setOpen(false);


    return (<>

        <div>


            <Button onClick={handleOpen} sx={{ textTransform: 'none', color: "white" }} varriant='text' >
 
                <Typography variant="subtitle2" sx={{ fontWeight: '400', }} >
                    {name}
                </Typography>

            </Button>

            <Modal
                open={open}

                onClose={handleClose}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description"
            >
                <Box sx={style}>


                    {
                        CheckLists.map((checklist) => {

                            return <Box key={checklist.id} my={3} sx={{ backgroundColor: '#04364A', borderRadius: '10px' }} >


                                <Box display={'flex'} alignItems={'center'} >
                                    <CheckBoxOutlinedIcon sx={{ mx: 2 }} fontSize='small' />
                                    <Typography variant='h6' > {checklist.name} </Typography>
                                    <IconButton onClick={()=>deleteChecklist(checklist.id)} sx={{ marginLeft: 'auto' }}>
                                        <DeleteOutlinedIcon  />
                                    </IconButton>
                                </Box>

                               
                                    <CheckItem checklist={checklist} ></CheckItem>
                            </Box>

                        })
                    }
                    <Box p={3} sx={{ backgroundColor: 'black', borderRadius: '10px' }} display={'flex'} alignItems={'center'}>
                        <TextField
                            sx={{ width: '70%' }}
                            label="Add CheckList"
                            id="standard-size-small"
                            value={ChecklistName}
                            onChange={(e) => { setChecklistName(e.target.value); }}
                            size="small"

                        />
                        <IconButton onClick={addChecklist} sx={{ marginLeft: 'auto' }} >
                            <AddIcon />
                        </IconButton>
                    </Box>
                </Box>
            </Modal>
        </div>
    </>);
}

export default CheckListModal;




