import { Modal } from "@mui/material";
import { useState } from "react";
import CreateBoardOverlay from "./CreateBoardOverlay";
import { Box, Typography } from "@mui/material";


function CreateBoard({ setboards, boards }) {
  const [open, setopen] = useState(false);
  

  const handleClose = () => { setopen(false) }

  return (<>

    <Box onClick={() => { setopen(true) }} display={'flex'} justifyContent={'center'} alignItems={'center'} sx={{ backgroundColor: '#A1BDD914' }} height='200px' width='400px' >

      <Typography variant="subtitle2">
        Create new board
      </Typography>
    </Box>

    < Modal open={open} onClose={handleClose} >
      <Box>
        <CreateBoardOverlay setboards={setboards} boards={boards} setopen={setopen} />
      </Box>
    </Modal >

  </>);
}

export default CreateBoard;