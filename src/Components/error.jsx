import { Box, Typography } from "@mui/material";

function Error({msg,color}) {
    return ( <>
    <Box justifyContent={"center"} width={'100vw'}>
    <Typography variant="h2"  p={5} m={5} color={color} > {msg} </Typography>
   </Box>    

    </> );
}

export default Error;