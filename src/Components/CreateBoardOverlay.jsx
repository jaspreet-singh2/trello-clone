import axios from "axios";
import { Box, Button, TextField } from "@mui/material";
import { useState } from "react";

const ApiandToken = import.meta.env.VITE_KEYANDTOKEN;

function CreateBoardOverlay({ boards, setboards, setopen }) {

  const style = {
    flexDirection: 'column',
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 400,
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
    p: 4,
  }
  const [Name, setName] = useState('');

  function CreateBoard() {
    axios.post(`https://api.trello.com/1/boards/?name=${Name}&${ApiandToken}`)
      .then((res) => {
        setopen(false)
        //console.log(res.data);
        setboards([...boards, res.data])
      }
      )
  }



  return (
    <Box p={3} component="div" sx={style} display={"flex"}>
      <TextField
        size="small"
        required
        value={Name}
        onChange={(e) => { setName(e.target.value) }}
        id="outlined-required"
        label="Board title"

        helperText=''
      />


      <Button onClick={CreateBoard} sx={{ margin: '1em' }} variant="contained" >
        Create
      </Button>
    </Box>
  );
}

export default CreateBoardOverlay;