import axios from "axios";
import { useEffect } from "react";
const ApiandToken = import.meta.env.VITE_KEYANDTOKEN;
import Card from "./Card";



function Cards({ listId, cards, SetCards }) {




    useEffect(() => {
        axios.get(`https://api.trello.com/1/lists/${listId}/cards?${ApiandToken}`)
            .then(res => {
                SetCards(res.data);
            })
    }, [listId])


    function deleteCard(id) {
        axios.delete(`https://api.trello.com/1/cards/${id}?${ApiandToken}`)
            .then(res => {
                SetCards(cards.filter(card => id !== card.id));
            });
    }


    return (
        <>
            {
                cards.map(card => <Card deleteCard={deleteCard} key={card.id} id={card.id} name={card.name}></Card>)
            }

        </>
    );
}

export default Cards;