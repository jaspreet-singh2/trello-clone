import AddCard from "./AddCard.jsx";
import AddIcon from "@mui/icons-material/Add";
import Cards from "./Cards";
import DeleteRoundedIcon from "@mui/icons-material/DeleteRounded";
import { Box, Divider, IconButton, Typography } from "@mui/material";
import { useState } from "react";

function List({ id, name, deleteList }) {


    const [cards, SetCards] = useState([])
    const [anchorEl, setAnchorEl] = useState(null);

    const handleClick = (event) => {
        setAnchorEl(event.currentTarget);
    };

    //console.log(`renderig list ${id}`)

    return (
        <>
            <Box p={3} display={"flex"} flexDirection={'column'} sx={{ width: '35ch', height: 'max-content', borderRadius: '10px', backgroundColor: 'black' }}  >
                <Box display={"flex"} sx={{ alignItems: 'center', width: '100%' }} >
                    <Typography variant="subtitle2" sx={{ fontWeight: '600' }} >
                        {name}
                    </Typography>

                    <IconButton onClick={() => { deleteList(id) }} size="small" sx={{ marginLeft: 'auto' }}>
                        <DeleteRoundedIcon />
                    </IconButton>

                </Box>
              
                <Divider sx={{ marginTop: '1em', marginBottom: '1em', backgroundColor: 'green' }} />
              
                <Cards cards={cards} SetCards={SetCards} listId={id} />
              
                <Box display={"flex"} sx={{ alignItems: 'center', width: '100%' }} >
                    <Typography variant="subtitle2" >
                        Create New Card
                    </Typography>
                    <IconButton onClick={handleClick} size="small" sx={{ marginLeft: 'auto' }} >
                        <AddIcon />
                    </IconButton>

                    <AddCard cards={cards} SetCards={SetCards} setAnchorEl={setAnchorEl} listId={id} anchorEl={anchorEl}></AddCard>
                </Box>
            </Box>

        </>
    );
}

export default List;