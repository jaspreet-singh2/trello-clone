import AddIcon from "@mui/icons-material/Add";
import Checkbox from "@mui/material/Checkbox";
import CloseIcon from "@mui/icons-material/Close";
import LinearProgress from "@mui/material/LinearProgress";
import axios from "axios";
import { Box, FormControl, FormControlLabel, FormGroup, IconButton, TextField, Typography } from "@mui/material";
import { styled } from "@mui/material/styles";
import { useEffect, useState } from "react";

const ApiandToken = import.meta.env.VITE_KEYANDTOKEN;
const BorderLinearProgress = styled(LinearProgress)(({ theme }) => ({
    height: 10,
    borderRadius: 5,

}));

// function checkItemsReducer(CheckItemsState,action)
// {

// }




function CheckItem({ checklist }) {

    const [CheckItemName, setCheckItemName] = useState('');

    const [CheckItemsState, setCheckItemsState] = useState(checklist.checkItems);

    const [progress, setprogress] = useState(0);
    const [Err, seterr] = useState(false);
    // //console.log(CheckItemsState
    const checked = CheckItemsState.filter((x) => x.state === 'complete')
    // //console.log(checked.length);




    function changeCheckboxState(CheckItem) {

        axios.put(`https://api.trello.com/1/cards/${checklist.idCard}/checkItem/${CheckItem.id}?${ApiandToken}&state=${CheckItem.state == "complete" ? 'incomplete' : 'complete'}`)
            .then((res) => {
                //console.log(res);
                setCheckItemsState(CheckItemsState.map(checkItem => checkItem.id !== CheckItem.id?checkItem:res.data))
                
            })

    }

    useEffect(() => {
        setprogress((checked.length / CheckItemsState.length) * 100)
       //  console.log((checked.length / CheckItemsState.length) * 100)
    }, [CheckItemsState])


    function AddCheckItem() {
        if(CheckItemName=='')
        {
            seterr(true);
            return;
        }
        axios.post(`https://api.trello.com/1/checklists/${checklist.id}/checkItems?name=${CheckItemName}&${ApiandToken}`)
            .then((res) => {
                //console.log(res.data);
                setCheckItemsState([...CheckItemsState, res.data]);
                setCheckItemName('');
            })
    }

    function deleteCheckItem(idCheckItem) {
        axios.delete(`https://api.trello.com/1/cards/${checklist.idCard}/checkItem/${idCheckItem}?${ApiandToken}`)
            .then((res) => {
                // //console.log(res)
                setCheckItemsState(CheckItemsState.filter(item => item.id !== idCheckItem));
            })
    }

    return (

        <FormGroup sx={{ backgroundColor: 'Black', p: 3, borderRadius: '10px' }} >

            {CheckItemsState.length === 0 ? '' : <Box>
                <Typography variant='body2'>{progress.toFixed(0)} %</Typography>
                <BorderLinearProgress variant="determinate" value={progress} color={progress === 100 ? 'success' : 'info'} />
            </Box>}

            <Box py={2} >
                {CheckItemsState.map((checkItem) => {

                    return (<Box key={checkItem.id} display={'flex'} alignItems={'center'}>
                        <FormControlLabel control={<Checkbox checked={checkItem.state == "complete" ? true : false} onChange={() => changeCheckboxState(checkItem)} />} label={checkItem.name} />

                        <IconButton onClick={() => deleteCheckItem(checkItem.id)} sx={{ marginLeft: 'auto' }}>
                            <CloseIcon fontSize='small' ></CloseIcon>
                        </IconButton>
                    </Box>)
                })
                }
            </Box>
            <Box noValidate autoComplete="off" display={'flex'} my={2} alignItems={'center'}>
                
                <TextField
                      required={true}
                      error={Err}
                    sx={{ width: '90%' }}
                    label="Add CheckItem"
                    id="standard-size-small"
                    value={CheckItemName}
                    onChange={(e) => { setCheckItemName(e.target.value); }}
                    size="small"
                    helperText={Err?'This field is required':''}

                />
                <IconButton type="submit" onClick={AddCheckItem} >
                    <AddIcon />
                </IconButton>
             
            </Box>
        </FormGroup>

    );
}

export default CheckItem;