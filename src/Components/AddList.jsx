import { Box, IconButton, Popover, Typography } from "@mui/material";
import AddIcon from '@mui/icons-material/Add';
import { useState } from "react";
import AddListPopover from "./AddListPopover";




function AddList() {


    const [anchorEl, setAnchorEl] = useState(null);

    const handleClick = (event) => {
        setAnchorEl(event.currentTarget);
    };

    
    const handleClose = () => {
        setAnchorEl(null);
    };

    const open = Boolean(anchorEl);
    const id = open ? 'simple-popover' : undefined;



    return (<>
        <Box p={3} display={"flex"} flexDirection={'column'} sx={{ width: '25ch', height: 'max-content', borderRadius: '10px', backgroundColor: 'black' }}  >
            <Box display={"flex"} sx={{ alignItems: 'center', width: '100%' }} >
                <Typography variant="subtitle2" >
                    Add New List
                </Typography>
                <IconButton onClick={handleClick} size="small" sx={{ marginLeft: 'auto' }} >
                    <AddIcon />
                </IconButton>


                <Popover
                    id={id}
                    open={open}
                    anchorEl={anchorEl}
                    onClose={handleClose}
                    anchorOrigin={{
                        vertical: 'bottom',
                        horizontal: 'left',
                    }}
                >
                    <AddListPopover setopen={handleClose}></AddListPopover>
                </Popover>

            </Box>
        </Box>
    </>);
}

export default AddList;