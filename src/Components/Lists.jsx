import AddList from "./AddList";
import List from "./List";
import axios from "axios";
import { Box } from "@mui/material";
import { createContext, useEffect, useState } from "react";
import { useParams } from "react-router-dom";

const ApiandToken = import.meta.env.VITE_KEYANDTOKEN;
export const ListsContext = createContext();

function Lists() {
      const { BoardId } = useParams()
      const [lists, Setlists] = useState([])

      //console.log('renderrering lists');

      function DeleteList(id) {
            axios.put(`https://api.trello.com/1/lists/${id}/closed?${ApiandToken}&value=true`)
            .then(res => {    
              //console.log(res.data);       
              Setlists(lists.filter(list => id !== list.id))
                  })
      }

      useEffect(() => {

            axios.get(`https://api.trello.com/1/boards/${BoardId}/lists?${ApiandToken}`)
                  .then(res => {
                        Setlists(res.data)
                        //console.log(res.data);
                  });

      }, [])

      return (

            <ListsContext.Provider value={[lists, Setlists]}>


                  <Box p={4} display={'flex'} sx={{ columnGap: '1em', height: '80vh', width: 'max-content' }}  >

                        {
                              lists.map((list) => <List key={list.id} deleteList={DeleteList} id={list.id} name={list.name} ></List>)

                        }

                        <AddList></AddList>
                  </Box>
            </ListsContext.Provider>
      );
}

export default Lists;