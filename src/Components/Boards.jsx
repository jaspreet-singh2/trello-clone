import Board from "./Board";
import CreateBoard from "./CreateBoard";
import Error from "./error";
import axios from "axios";
import { Box } from "@mui/material";
import { useEffect, useState } from "react";

const ApiandToken = import.meta.env.VITE_KEYANDTOKEN;

function Boards() {
    const [boards, setboards] = useState([]);
  const [err,seterr]=useState(false)
  const [loading,setloading]=useState(true)
    useEffect(() => {
        axios.get(`https://api.trello.com/1/organizations/651b92bb5673d79e74e791f3/boards?${ApiandToken}`)
            .then((response) => {
                setboards(response.data);
                seterr(false);
                setloading(false);
                //console.log(response.data);
            })
            .catch(()=>seterr(true))
    }, [])

     if(err)
     {
        return <Error msg={'Invalid API Key'} color={'error'}></Error>
     }

     if(loading)
     {
        return <Error msg={'Loading...'} color={'info'}></Error>
     }

    return (
       
      
        <Box padding={5} justifyContent="space-around" display={'flex'} sx={{ rowGap: '1em', width: '100vw', flexWrap: 'wrap' }} >
            {boards.map((Boardo) => {
                return (
                    <Board id={Boardo.id} key={Boardo.id} url={Boardo.prefs.backgroundImageScaled ? Boardo.prefs.backgroundImageScaled[2].url : ''} BgColor={Boardo.prefs.backgroundColor} name={Boardo.name}></Board>
                );
            })}
            <CreateBoard boards={boards} setboards={setboards} ></CreateBoard>
        </Box>
      
    );
}

export default Boards;