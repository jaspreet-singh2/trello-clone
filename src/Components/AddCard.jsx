import axios from "axios";
import { Box, Button, Popover, TextField } from "@mui/material";
import { useState } from "react";

const ApiandToken = import.meta.env.VITE_KEYANDTOKEN;
function AddCard({ cards, SetCards, listId, anchorEl, setAnchorEl }) {



  const handleClose = () => {
    setAnchorEl(null);
  };

  const open = Boolean(anchorEl);
  const idO = open ? 'simple-popover' : undefined;



  const [Name, setName] = useState('');

  function CreateCard() {
    axios.post(`https://api.trello.com/1/cards?idList=${listId}&${ApiandToken}&name=${Name}`)
      .then((res) => {
        handleClose()
        //console.log(res.data);
        SetCards([...cards, res.data])
      }
      )
  }



  return (<>

    <Popover
      id={idO}
      open={open}
      anchorEl={anchorEl}
      onClose={handleClose}
      anchorOrigin={{
        vertical: 'bottom',
        horizontal: 'left',
      }}>
      <Box p={3} component="div" sx={{ flexDirection: 'column' }} display={"flex"}>
        <TextField
          size="small"
          required
          value={Name}
          onChange={(e) => { setName(e.target.value) }}
          id="outlined-required"
          label="Card Name"
          helperText=''
        />


        <Button onClick={CreateCard} sx={{ margin: '1em' }} variant="contained" >
          ADD
        </Button>
      </Box>
    </Popover>
  </>);
}

export default AddCard;