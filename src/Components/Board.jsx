import { Card, CardContent, CardMedia, Typography } from "@mui/material";
import { Link } from "react-router-dom";

function Board({ name, url, BgColor, id }) {

  return (<>

    <Link to={`/boards/${id}`}>
      <Card sx={{ position: 'relative', margin: '0 2% 2% 0' }}>
        
        <CardMedia sx={{ backgroundColor: BgColor, width: '100%', height: '100%', position: "absolute", top: 0, }}
          component={BgColor ? 'div' : "img"}
          alt="green iguana"
          height="140"
          image={url}
        />

        <CardContent sx={{ position: 'relative', height: '200px', width: '400px', top: '0px', 'paddingTop': '0.1em' }} >
         
          <Typography sx={{ fontWeight: 700 }} variant="subtitle1" component="div">
            {name}
          </Typography>

        </CardContent>


      </Card>

    </Link>



  </>);
}

export default Board;