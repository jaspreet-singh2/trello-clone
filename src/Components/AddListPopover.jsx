import axios from "axios";
import { Box, Button, TextField } from "@mui/material";
import { useContext, useState } from "react";
import { useParams } from "react-router-dom";
import { ListsContext } from "./Lists";

const ApiandToken = import.meta.env.VITE_KEYANDTOKEN;
function AddListPopover({ setopen }) {


    const { BoardId } = useParams()
    const [Name, setName] = useState('');
    const [Lists, Setlists] = useContext(ListsContext);

    function CreateList() {
        axios.post(`https://api.trello.com/1/lists?name=${Name}&idBoard=${BoardId}&${ApiandToken}`)
            .then((res) => {
                setopen()
                //console.log(res.data);
                Setlists([...Lists, res.data])
            }
            )
    }



    return (<>
        <Box p={3} component="div" sx={{ flexDirection: 'column' }} display={"flex"}>
            <TextField
                size="small"
                required
                value={Name}
                onChange={(e) => { setName(e.target.value) }}
                id="outlined-required"
                label="List Name"
                helperText='' />


            <Button onClick={CreateList} sx={{ margin: '1em' }} variant="contained" >
                Create
            </Button>
        </Box>
    </>);
}

export default AddListPopover;