import CheckListModal from "./ModalChecklist";
import CloseIcon from "@mui/icons-material/Close";
import { Box, IconButton } from "@mui/material";

function Card({ name, id, deleteCard }) {


  
    return (
        <Box display={"flex"} sx={{ borderRadius: '5px', boxShadow: 3, my: 1, p: 2, backgroundColor: 'rgb(29,33,37)', alignItems: 'center', width: '100%' }} >

           
             <CheckListModal id={id}  name={name} />

            <IconButton onClick={() => deleteCard(id)} size="small" sx={{ marginLeft: 'auto' }} >
                <CloseIcon />
            </IconButton>

        </Box>
    );
}

export default Card;