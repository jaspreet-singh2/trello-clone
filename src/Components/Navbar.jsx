import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import SearchIcon from "@mui/icons-material/Search";
import ViewCompactIcon from "@mui/icons-material/ViewCompact";
import { AppBar, Box, Button, IconButton, InputAdornment, TextField, Toolbar } from "@mui/material";

import{Link} from 'react-router-dom'
function Navbar() {
  return (<>

 
    <AppBar sx={{ p: 1 }} position="static">
      <Toolbar sx={{ flexGrow: 1 }} variant="dense">
        <IconButton edge="start" color="inherit" aria-label="menu" sx={{ mr: 2 }}>
          <ViewCompactIcon />
        </IconButton>
       
        <Box  component={'img'} width={80} src="https://trello.com/assets/87e1af770a49ce8e84e3.gif" >
        </Box>
        <Link to='/'>
        <Button sx={{ color: "grey", marginLeft: '1%' }} endIcon={< ExpandMoreIcon />}>
          Workspaces
        </Button>
        </Link>
        <TextField size="small"
          label=""
          id="outlined-start-adornment"
          sx={{ marginLeft: 'auto', width: '25ch', }}
          InputProps={{
            style:
            {
              borderRadius: '50px'
            },
            startAdornment: <InputAdornment position="start"><SearchIcon /></InputAdornment>,
          }}
        />

      </Toolbar>


    </AppBar>


  </>);
}

export default Navbar;